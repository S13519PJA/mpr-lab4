package unityofwork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import domain.Entity;
import domain.EntityState;

public class HsqlUnitOfWork implements UnitOfWork {

	private Connection connection;
	private Map<Entity, UnitOfWorkRepository> entities =
	new LinkedHashMap<Entity, UnitOfWorkRepository>();
	
	public HsqlUnitOfWork(Connection connection) {
		super();
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void savechanges() {
		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case Changed:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case New:
				entities.get(entity).persistAdd(entity);
				break;
			case Unchanged:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
		} 
			
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
	}

	public void undo() {
		entities.clear();
		
	}

	public void markAsNew(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.New);
		entities.put(entity, repo);
		
	}

	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repo);

	}

	public void markAsChanged(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.Changed);
		entities.put(entity, repo);	
		
	}
}
