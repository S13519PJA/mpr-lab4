package repositories;


public interface RepositoryCatalog {
	
	public EnumerationValueRepository enumerations();
	public UserRepository users();

}
