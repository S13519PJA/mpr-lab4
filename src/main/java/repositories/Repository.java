package repositories;


public interface Repository<TEntity> {
	
	public TEntity withId (int Id);
	public TEntity allOnPage (PagingInfo page);
	public void add (TEntity entity);
	public void delete (TEntity entity);
	public void modify (TEntity entity);
	public int count ();

}
