package repositories;


public class HsqlRepositoryCatalog implements RepositoryCatalog {
	
	private DataBase database;
	
	public HsqlRepositoryCatalog (DataBase database) {
		this.database = database;
	}

	public EnumerationValueRepository enumerations() {
		return (EnumerationValueRepository) database.getEnumerationValue();
	}

	public UserRepository users() {
		return (UserRepository) database.getUser();
	}

}
