package repositories;


import domain.User;

public interface UserRepository extends Repository  {

	public User withLogin (String login);
	public User withLoginAndPassword (String login, String password);
	public void setupPermissions (User user);
}
